import React, { useState } from "react";
import Pokemon from "../../components/pokemon";
import RickAndMorty from "../../components/rick-and-morty";

const Characters = ({
  setCharacters,
  characters,
  setWhatRender,
  whatRender,
}) => {
  const [personagem, setPersonagem] = useState(true);

  const conditionalRender = () => {
    setPersonagem(!personagem);
    if (personagem === true) {
      setWhatRender("pokemon");
    } else {
      setWhatRender("rick-and-morty");
    }
  };
  return (
    <div>
      <button onClick={() => conditionalRender()}>
        {personagem ? "Pokemon" : "Rick and Morty"}
      </button>
      {personagem ? (
        <RickAndMorty
          whatRender={whatRender}
          typePersonageRender={personagem}
          setCharacters={setCharacters}
          characters={characters}
        />
      ) : (
        <Pokemon
          whatRender={whatRender}
          typePersonageRender={personagem}
          setCharacters={setCharacters}
          characters={characters}
        />
      )}
    </div>
  );
};

export default Characters;
