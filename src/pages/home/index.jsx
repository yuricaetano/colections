import React, { useState } from "react";
import { notification } from "antd";
import CharacterList from "../../components/character-list";

const Home = ({ characters, setCharacters, setWhatRender, whatRender }) => {
  const [conditionalRender, setConditionalRender] = useState([]);

  const showRickandMorty = () => {
    setWhatRender("rick-and-morty");
    setConditionalRender(
      characters.filter((character) => character.image !== undefined)
    );
  };
  const showPokemon = () => {
    setWhatRender("pokemon");
    setConditionalRender(
      characters.filter((character) => character.image === undefined)
    );
  };
  const showAll = () => {
    setWhatRender("all");
    setConditionalRender(characters);
  };
  const handleOnSelect = ({ name }) => {
    notification.info({
      key: name,
      message: "Good!",
      description: "Character removed!",
    });
    setCharacters(characters.filter((character) => character.name !== name));
  };
  let message = "";
  if (conditionalRender.length === 0) {
    message = `No characters chosen yet ${whatRender}`;
  }

  return (
    <div>
      <p>Select which characters you want to see</p>
      <button style={{ background: "blue" }} onClick={() => showRickandMorty()}>
        Rick and Morty
      </button>
      <button style={{ background: "blue" }} onClick={() => showPokemon()}>
        Pokemon
      </button>
      <button style={{ background: "blue" }} onClick={() => showAll()}>
        All
      </button>

      <CharacterList
        whatRender={whatRender}
        personageToRender={whatRender}
        onSelect={handleOnSelect}
        header="Your collection!"
        characters={conditionalRender}
      />
      {message}
    </div>
  );
};

export default Home;
