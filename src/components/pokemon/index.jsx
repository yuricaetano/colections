import React, { useEffect, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { notification } from "antd";
import styled from "styled-components";
import Characterlist from "../character-list";

const Pokemon = ({
  setCharacters,
  characters,
  typePersonageRender,
  whatRender,
}) => {
  const { page } = useParams();
  const history = useHistory();
  const [charactersAPI, setCharactersAPI] = useState([]);

  const handleOnSelect = (newCharacter) => {
    const alreadyAdd = characters.some(
      ({ name }) => name === newCharacter.name
    );

    if (alreadyAdd) {
      return notification.error({
        key: newCharacter.name,
        message: "Error",
        description: "Character has already been added!",
      });
    }

    notification.success({
      key: newCharacter.name,
      message: "Good!",
      description: "Added character!",
    });

    setCharacters([...characters, newCharacter]);
  };

  useEffect(() => {
    if (page < 1) return history.push("/characters/1");

    fetch(`https://pokeapi.co/api/v2/pokemon?limit=150`)
      .then((res) => res.json())
      .then(({ results }) => setCharactersAPI(results || []));
  }, [history, page, setCharactersAPI]);

  return (
    <Characterlist
      whatRender={whatRender}
      onSelect={handleOnSelect}
      characters={charactersAPI}
      typePersonageRender={typePersonageRender}
    />
  );
};

export default Pokemon;

const StyledControl = styled.div`
  padding: 10px;
  max-width: 500px;
  display: flex;
  width: 100%;
  justify-content: space-between;
`;
